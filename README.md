# systemback.sh

**System backup and restore script for Debian-based distributions**

This Bash script is requires a basic knowledge of the Linux command line interface!  
By default, all restore points in a same storage slot are incremental and temporary. The oldests may be automatically removed when the new ones are created.  

## Table of contents

- [Requirements](#requirements)
- [Install](#install)
- [Change configuration settings](#change-configuration-settings)
- [Usage](#usage)
- [Uninstall](#uninstall)

## Requirements

Make sure that the following packages are installed in the system:

*attr  
psmisc  
rsync*

```sh
sudo apt install attr psmisc rsync
```

## Install

Place the *systemback.sh* under the */usr/local/bin* directory and make it executable.

```sh
sudo wget -O /usr/local/bin/systemback.sh goo.gl/SXWYn7
sudo chmod +x /usr/local/bin/systemback.sh
```

The script basically a command line tool, but there is a clickable icon to quick-start the restore point creation and the restoration (from right-click menu).

```sh
sudo apt install xterm
mkdir -p ~/.local/share/{applicati,ic}ons
wget -O ~/.local/share/applications/systemback.desktop goo.gl/Vhe3z7
wget -O ~/.local/share/icons/systemback.png goo.gl/ms96CR
```

## Change configuration settings

*storage_dir  
definable_slots  
max_rp_num  
max_size  
exclude*

```sh
sudo nano /usr/local/bin/systemback.sh
```

![](https://prohardver.hu/dl/upc/2019-12/180556_nano.png)

Press *Ctrl+O* to save the changes, and *Ctrl+X* to exit from nano.

## Usage

Run the *systemback.sh* with an option.

```sh
sudo systemback.sh OPTION
```

The default slot in the storage directory is *0*. If the *definable_slots* is enabled, the storage slot is definable with the SBSLOT environment variable.

```sh
sudo SBSLOT="0" systemback.sh OPTION
```

Available options:

```
 -n, --new [NAME]           create a new restore point
 -s, --storage              print the current storage directory path and the slot
 -l, --list                 list available restore points
 -z, --size [INDEX]         calculate the incremental size of the storage slot or the apparent size of a restore point
 -r, --restore [INDEX]      perform a system and/or user's configuration files restoration
 -m, --repair [INDEX]       same as -r but the target (root) directory will be the '/mnt' instead of the '/'
 -e, --rename INDEX [NAME]  rename a restore point
 -k, --keep INDEX           set the restore point to be manually removable only
 -d, --remove [INDEX]       manually remove a restore point
```

Broken system files can be repaired from a Live system. Just manually mount the system partition(s) under the */mnt* directory, and use the *-m* option.

```sh
sudo bash -c "$(wget -O- goo.gl/SXWYn7)" . -m
```

![](https://prohardver.hu/dl/upc/2019-12/180556_restore.png)

## Uninstall

Just remove the previously installed files.

```sh
sudo rm /usr/local/bin/systemback.sh ~/.local/share/{applications/systemback.desktop,icons/systemback.png}
```

To remove all restore points, use the following commands:

```sh
cd /storage/directory
sudo chattr -Rfi SB_*
sudo rm -rf SB_*
```
